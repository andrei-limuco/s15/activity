console.log("Hello World");


let num1 = parseInt(prompt("Please input a number"));
let num2 = parseInt(prompt("Please input another number"));
let total = (num1 + num2);

if (total < 10) {
	console.warn(total);
}
else if (total >= 10 && total <= 19) {
	alert(num1 - num2);
}
else if (total >= 21 && total <= 29) {
	alert(num1 * num2);
}
else if (total >= 30) {
	alert(num1 / num2);
} else {
	console.log("Please input a valid number")
}


let name = prompt ("What is your name?");
let age = prompt("What is your age?");

if (name == "" || name == null || age == "" || age === null) {
	alert("Are you a time traveler?");
}
else {
	alert("Your name is " + name + " and your age is " + age);
}


switch (age) {
	case "18":
		alert("You are allowed to party.");
		break;
	case "21":
		alert("You are now part of the adult society.");
		break;
	case "65":
		alert("We thank you for your contribution to society.");
		break;
	default:
		alert("Are you sure you're not an alien?")
		break;
}


function isLegalAge() {

	if (age >= 18) {
		alert("You are of legal age.");
	} else {
		alert("You are not allowed here.")
	}

	try {
		//attempts to execute a code
		alerat(isLegalAge())
	}

	catch(error) {
		//handle the error
		console.warn(error.message)
	}

	finally {
		//regardless of the result, this will execute
		alert('Another message.')
	}
}

isLegalAge();